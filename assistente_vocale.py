from random import choice
import webbrowser
from pyttsx3 import init ;
from speech_recognition import Recognizer , Microphone;


# Motore per l'assistente
engine = init()
voices = engine.getProperty("voices")
engine.setProperty("voice", voices[0].id)
engine.say("Ciao sono elsa ,come posso aiutarti?")
engine.runAndWait()

#Permette di stmpare le verie voci;

#for voice in voices :
    #print(voice)

#riconoscitore vocale

r = Recognizer()


#Acceso microfono e comprensione testo 
with Microphone() as source:
    print("Pronto ad ascoltare .....")
    audio = r.listen(source)
    testo = r.recognize_google(audio, language="it-IT").lower()
    print(testo)

#Risposte al testo 
if "ricetta" in testo:
    with open("ricetta.txt", "w") as f:
        f.write("scemo chi legge")
    risposta = "ho cercsato per te, un testo con le ricette"
elif any(parola in testo for parola in ["ore", "ora", "orario"]):
    webbrowser.open("www.amazon.it/s?k=orologio")
    risposta = "vai a comprarti un orologio"
elif testo.startswith(("cosa", "come", "quanto")):
    risposta = choice(["ma che ne so", "chiedi ad alexa", "che ne so "])
engine.say(risposta)
engine.runAndWait()